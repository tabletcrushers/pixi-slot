export default class MathUtils {
	static lerp (start, end, amt) {
		
		return (1 - amt) * start + amt * end;
	}

	static getRandomInt(min, max) {
		min = Math.ceil(min);
		max = Math.floor(max);
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}
}