export const elastic = {
	in(k) {
		if (k == 0) return 0;
		if (k == 1) return 1;
		return -Math.pow(2, 10 * (k -= 1)) * Math.sin((k - 0.1) * (2 * Math.PI) / 0.4);
	},

	out(k) {
		if (k == 0) return 0;
		if (k == 1) return 1;
		return Math.pow(2, -10 * k) * Math.sin((k - 0.1) * (2 * Math.PI) / 0.4) + 1;
	},

	inOut(k)
	{
		if ((k *= 2) < 1) return -0.5 * Math.pow(2, 10 * (k -= 1)) * Math.sin((k - 0.1) * (2 * Math.PI) / 0.4);
		return Math.pow(2, -10 * (k -= 1)) * Math.sin((k - 0.1) * (2 * Math.PI) / 0.4) * 0.5 + 1;
	}
};


export const quadratic = {
	in (k) {

		return k * k;
	},

	out(k) {
		return k * (2 - k);
	},

	inOut(k) {
		if ((k *= 2) < 1) return 0.5 * k * k;
		return -0.5 * ((k -= 1) * (k - 2) - 1);
	}
};