export default class ObjectPool {

    constructor(creator, initialCount = 10) {
        this._creator = creator;
        this._pool = [];

        this._init(initialCount);
    }

    get size() {
        return this._pool.length;
    }

    get() {
        if (this._pool.length === 0) {
            return this._createObject();
        }

        return this._pool.pop();
    }

    put(obj) {
        this._pool.push(obj);
    }

    _init(initialCount) {
        while (initialCount > 0) {
            this.put(this._createObject());

            initialCount--;
        }
    }

    _createObject() {
		
		return this._creator.create();
    }
}