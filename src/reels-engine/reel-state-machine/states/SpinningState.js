import Notificator from '../../../notificator/Notificator.js';

export default class SpinningState {
	constructor(velocity) {
		this._velocity = velocity;
		this.lifeState = 'stopped';
		this.name = 'Spinning';
		this._spinResult;
	}

	onEnter(pointer) {
		this._spinResult = null;
		this.lifeState = 'started';
		this._spinResultReceived = false;
		this._reelStopping = false;
		this._subscribe();
	}

	onExit() {
		this._unsubscribe();
	}

	_subscribe() {
		Notificator.instance.addListener('spinResultReceived', this._onResultReceived, this);
	}

	_unsubscribe() {
		Notificator.instance.removeListener('spinResultReceived', this._onResultReceived, this);
	}

	calculate(pointer, delta) {

		return pointer + this._velocity;
	}

	update(reel, delta) {
		if (!this._spinResult) {

			return;
		}

		reel.tryToClearSpinResult();

		if(reel.circulationJustDone && !this._reelStopping) {
			reel.stop(this._spinResult[reel.id].textureName, this._spinResult[reel.id].winLine);
			this._reelStopping = true;
		}
	}

	stop() {
		this.lifeState = 'stopped';
	}

	_onResultReceived(result) {
		this._spinResult = result;
	}
}