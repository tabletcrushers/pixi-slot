export default class IdleState {
	constructor(name) {
		this.lifeState = 'stopped';
		this.name = 'Idle';
	}

	calculate(pointer) {
		
		return pointer;
	}

	onEnter(pointer) {
		this.lifeState = 'started';
	}

	stop() {
		this.lifeState = 'stopped';
	}

	update() {
		
	}
}