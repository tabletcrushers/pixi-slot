import MathUtils from '../../../utils/MathUtils.js';
import {elastic} from '../../../utils/easings/Easings.js';
import Notificator from '../../../notificator/Notificator.js';

export default class DecelerationState {
	constructor(duration) {
		this._time = 0;
		this.lifeState = 'stopped';
		this.name = 'Deceleration';
		this._duration = duration;
		this._easing = elastic.out;
	}

	onEnter() {
		this.lifeState = 'started';
		this._time = 0;
	}

	update(reel, delta) {
		this._time += delta / this._duration;
		
		if (this._time >= 1) {
			this.stop();

			return this._finishValue;
		}
	}

	calculate(pointer, delta) {
		

		const step = this._easing(this._time);
		
		return MathUtils.lerp(this._startValue, this._finishValue, step);
	}

	setPath(startValue, finishValue) {
		this._startValue = startValue;
		this._finishValue = finishValue;
	}

	stop() {
		this.lifeState = 'stopped';
		Notificator.instance.notify('reelStopped');
	}
}