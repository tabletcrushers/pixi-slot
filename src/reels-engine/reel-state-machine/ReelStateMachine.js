export default class ReelStateMachine {
	constructor(states, initialStateName) {
		this._states = states;

		this._currentState = this._states.get(initialStateName);
		this._currentState.onEnter();
	}

	get reel() {

		return this._reel;
	}

	set reel(reel) {
		this._reel = reel;
	}

	get currentState() {
		
		return this._currentState;
	}

	_addStates() {
		this._states.set('Idle', new IdleState());
		this._states.set('Spinning', new SpinningState());
		this._states.set('Deceleration', new DecelerationState(this._viewportGeometry));
	}

	update(reel, delta) {
		if (this._currentState.lifeState != 'started') {
			this._switchState();
		}
		this._currentState.update(reel, delta);
	}

	switchState(name) {
		this._currentState.stop();
		this._currentState = this._states.get(name);
		this._currentState.onEnter();

		if (name === 'Deceleration') {
			this._currentState.setPath(this._reel.pointer, this._reel.winPointer);
		}
	}

	_switchState() {
		let newName;
		switch(this._currentState.name) {
			case 'Deceleration':
			newName = 'Idle';
			break;
		}

		this._currentState = this._states.get(newName);
		this._currentState.onEnter();
	}
}