export default class ReelItem {

    constructor(x, y, geometry, textureName) {
        this.x = x;
		this.y = y;
		this.geometry = geometry;
		this.textureName = textureName;
		this.originTextureName;

		this.renderTransform = {x: 0, y: 0};
	}

	clone() {
		
		return new ReelItem(this._x, this._y, this.geometry, this.textureName);
	}
}