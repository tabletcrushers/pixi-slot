import Reel from '../Reel.js';
import Reels from '../Reels.js';
import ReelItem from '../ReelItem.js';
import ReelStateMachine from '../reel-state-machine/ReelStateMachine.js';
import IdleState from '../reel-state-machine/states/IdleState.js';
import SpinningState from '../reel-state-machine/states/SpinningState.js';
import DecelerationState from '../reel-state-machine/states/DecelerationState.js';

export default class ReelsBuilder {
	build(config) {
		const reels = [
			new Reel(0, this._createItems(), this._createStateMachine(), 0, 0, 0),
			new Reel(1, this._createItems(), this._createStateMachine(), 140, 0, 500),
			new Reel(2, this._createItems(), this._createStateMachine(), 280, 0, 1000)
		];

		return new Reels(reels, 21, 21);
	}

	_createItems() {
		return [
			new ReelItem(0, 0, 120, '1xbar'), 
			new ReelItem(0, 120, 120, '2xbar'),
			new ReelItem(0, 240, 120, '3xbar'), 
			new ReelItem(0, 360, 120, 'cherry'), 
			new ReelItem(0, 480, 120, 'seven')
		];
	}

	_createStateMachine() {
		const states = new Map();
		states.set('Idle', new IdleState());
		states.set('Spinning', new SpinningState(-28));
		states.set('Deceleration', new DecelerationState(80));
		return new ReelStateMachine(states, 'Idle');
	}
}