export default class Reel {
    constructor(id, strip, stateMachine, x, y, stopDelay = 500) {
		this._stopDelay = stopDelay;
		this._id = id;
		this._x = x;
		this._y = y;
        this._viewportGeometry = 240;
		this._pointer = 0;
		this._stateMachine = stateMachine;
		stateMachine.reel = this;
		this._strip = strip;

		this._reelGeometry = this._calculateReelGeometry();
		this._lastItem = this._strip[this._strip.length - 1];

		this._originStripLenght = strip.length;
		this._extendStrip();

		this._isSpinning = false;
	}

	get id() {

		return this._id;
	}

	get pointer() {

		return this._pointer;
	}

	get winPointer() {

		return this._winPointer;
	}

	get readyToStop() {

		return this._strip.indexOf(this._getHead()) === this._lastItem;
	}
	
	update(delta) {
		this._pointer = this._stateMachine.currentState.calculate(this._pointer, delta);
		this._pointer = this._circulatePointer(this.pointer);
		this._stateMachine.update(this, delta);

		if (this.circulationJustDone) {
			this.circulationJustDone = false;
		}
	}

	getItemRenderPosition(index) {
		this._strip[index].y - this._pointer;
	}

	getVisibleItems() {
		const visibleItems = this._strip.filter(
			item => this._checkViewportIntersection(item)
		);

		this._setRenderTransform(visibleItems);
		
		return visibleItems;
	}

	stop(textureName, line) {
		
		setTimeout(() => {
			this._delayedStop(textureName, line);
			this._stateMachine.switchState('Deceleration');
		}, this._stopDelay);
	}

	start() {
		this._stateMachine.switchState('Spinning');
	}

	tryToClearSpinResult() {
		if (Boolean(this._winItem)) {
			if(this._pointer + this._viewportGeometry < this._strip[this._originStripLenght].y) {
				this._winItem.textureName = this._winItem.originTextureName;
				this._strip[this._originStripLenght].textureName = this._winItem.originTextureName;
				this._winItem.originTextureName = null;
				this._strip[this._originStripLenght].originTextureName = null;
				this._winItem = null;
			}
		}
	}

	_delayedStop(textureName, line) {
		this._winItem = this._strip[0];
		this._winItem.originTextureName = this._winItem.textureName;
		this._strip[this._originStripLenght].originTextureName = this._winItem.textureName;
		this._winItem.textureName = textureName;
		this._strip[this._originStripLenght].textureName = textureName;
		

		let offset = 0;
		switch (line) {
			case 'middle': 
				offset  = -this._winItem.geometry / 4;
				break;
			case 'bottom':
				offset = -this._winItem.geometry / 2;
		}

		this._winPointer = offset;
	}

	_extendStrip() {
		let i = 0;
		let fullGeometry = this._reelGeometry;

		do {
			const item = this._strip[i].clone();
			item.y = fullGeometry;
			fullGeometry += item.geometry;
			this._strip.push(item);
			i++;
		} while (fullGeometry - this._reelGeometry < this._viewportGeometry);

		this._fakeItemsAmount = i;
	}

	_calculateReelGeometry() {

		return this._strip.reduce((geometry, b) => geometry + b.geometry, 0);
	}

	_setRenderTransform(items) {
		for (const item of items) {
			item.renderTransform.y = item.y - this._pointer + this._y;
			item.renderTransform.x = this._x;
		}
	}

	_circulatePointer(pointer) {
		if (pointer < 0) {
			pointer += this._reelGeometry; 
			this.circulationJustDone = true;
		}

		return pointer;
	}

	_checkViewportIntersection(reelItem) {
		if (reelItem.y <= this._pointer && reelItem.y + reelItem.geometry > this._pointer) {

			return true;
		}

		if (reelItem.y >= this._pointer && reelItem.y < this._pointer + this._viewportGeometry) {

			return true;
		}

		return false;
	}

	_getTail() {
		const visibleItems = this.getVisibleItems();
		const bottom = this._pointer + this._viewportGeometry;

		for (const item of visibleItems) {
			if (bottom > item.y && bottom <= item.y + item.geometry) {
				return item;
			}
		}
	}

	_getHead() {
		const visibleItems = this.getVisibleItems();
		const bottom = this._pointer + this._viewportGeometry;

		for (const item of visibleItems) {
			if (this._pointer >= item.y && this._pointer < item.y + item.geometry) {
				return item;
			}
		}
	}
}