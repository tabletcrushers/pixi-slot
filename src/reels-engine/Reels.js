import Notificator from '../notificator/Notificator.js';

export default class Reels {
	constructor(reels, x, y, delay = 280) {
		this._reels = reels;
        this._x = x;
		this._y = y;
		this._delay = delay;
		this._stoppedRealsAmount = 0;

		this._subscribe();
	}

	getVisibleItems() {
		const visibleItems = [];
		for (const reel of this._reels) {
			for (const item of reel.getVisibleItems()) {
				item.renderTransform.x += this._x;
				item.renderTransform.y += this._y;
				visibleItems.push(item);
			}
		}

		return visibleItems;
	}

	update(delta) {
		for (const reel of this._reels) {
			reel.update(delta);
		}
	}

	_subscribe() {
		Notificator.instance.addListener('spinResultReceived', this._onSpinResultReceived, this);
		Notificator.instance.addListener('reelsStarted', this._onReelsStarted, this);
		Notificator.instance.addListener('reelStopped', this._onReelStopped, this);
	}

	_onReelStopped() {
		this._stoppedRealsAmount++;
		if (this._stoppedRealsAmount === this._reels.length) {
			Notificator.instance.notify('reelsStopped');
			this._stoppedRealsAmount = 0;
		}
	}

	_onSpinResultReceived(result) {
		for (const i in this._reels) {
			const reel = this._reels[i];
		}
	}

	_onReelsStarted() {
		for (const i in this._reels) {
			const reel = this._reels[i];
			setTimeout(() => { reel.start(); }, this._delay * i);
		}
	}
}