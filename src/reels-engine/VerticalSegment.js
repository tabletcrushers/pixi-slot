export default class VerticalSegment {

    constructor(beginPoint, endPoint) {
        this.beginPoint = beginPoint;
        this.endPoint = endPoint;
    }

    intersects(geometry) {
        return(
            (geometry.beginPoint.y >= this.beginPoint.y && geometry.beginPoint.y < this.endPoint.y) ||
            (geometry.endPoint.y > this.beginPoint.y && geometry.endPoint.y <= this.endPoint.y) ||
            (geometry.beginPoint.y <= this.beginPoint.y && geometry.endPoint.y >= this.endPoint.y)
        );
    }
}