import { Sprite, Container, Graphics } from '../../pixi.js';
import ObjectPool from '../utils/ObjectPool.js';
import Notificator from '../notificator/Notificator.js';

const itemsWidth = 140;

export default class RenderEngine {

    constructor(stage, resources) {
		const reelItemsCreator = {};
		reelItemsCreator.create = () => new Sprite();
		this._pool = new ObjectPool(reelItemsCreator);
		this._renderedItems = [];
		this._resources = resources;
		this._stage = stage;
		this._reelItemsNode = new Container();
		this._stage.addChild(this._reelItemsNode);
		this._winLineNode = new Container();
		this._stage.addChild(this._winLineNode);
		this._drawBorder(20);
		this._drawBorder(202);

		this._createMask();
		this._subscribe();
	}

	renderReelItems(visibleItems) {
		this._visibleItem = visibleItems;

        for (const item of this._renderedItems) {
            item.visible = false;
		}

        this._renderedItems.length = 0;

        visibleItems.forEach((item) => {
            const renderItem = this._getReelItem(item.textureName);
			renderItem.x = item.renderTransform.x;
			renderItem.y = item.renderTransform.y;
			renderItem.visible = true;

			if (!Boolean(renderItem.parent)) {
				this._reelItemsNode.addChild(renderItem);
			}

			renderItem.visible = true;

            this._renderedItems.push(renderItem);
        });
	}

	_clear() {
		this._winLineNode.removeChildren();
	}

	_subscribe() {
		Notificator.instance.addListener('win', this._drawWinLine, this);
		Notificator.instance.addListener('reelsStarted', this._clear, this);
	}
	
	_createMask() {
		const graphics = new Graphics();
		graphics.beginFill(0xFF3300);
		graphics.drawRect(21, 21, 420, 180);
		graphics.endFill();

		this._reelItemsNode.mask = graphics;
	}

	_getReelItem(textureName) {
		const item = this._pool.get();
		item.texture = this._resources[textureName].texture;

        return item;
	}

	_drawBorder(y) {
		const line = new Graphics();
		line.lineStyle(1, 0xffbb0c);
		line.moveTo(22, y);
		line.lineTo(440, y);
		this._stage.addChild(line);
	}
	
	_drawWinLine() {
		const points = [];
		for (const item of this._visibleItem) {
            if (Boolean(item.originTextureName)) {
				points.push({ x: item.renderTransform.x + itemsWidth / 2, y: item.renderTransform.y + item.geometry / 2 });
			}
		}
		
		const line = new Graphics();
		line.lineStyle(7, 0xffffff);

		const innerLine = new Graphics();
		innerLine.lineStyle(3, 0x920000);
		
		const startPoint = points.shift();
		line.moveTo(startPoint.x, startPoint.y);
		innerLine.moveTo(startPoint.x, startPoint.y);
		
		for (const point of points) {
			line.lineTo(point.x, point.y);
			innerLine.lineTo(point.x, point.y);
		}

		this._winLineNode.addChild(line);
		this._winLineNode.addChild(innerLine);
	}
}