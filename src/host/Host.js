import RenderEngine from '../render-engine/RenderEngine.js';
import ReelsBuilder from '../reels-engine/builders/ReelsBuilder.js';
import ActionPanel from '../action-panel/ActionPanel.js';
import Paytable from '../paytable/Paytable.js';
import SpinToPaytableResult from '../paytable/SpinToPaytableResult.js';

export default class Host {

    constructor(stage, recources) {
		this._recources = recources;
		this._renderEngine = new RenderEngine(stage, recources);
		this._stage = stage;

		this._repository = new Map();

		const builder = new ReelsBuilder();
		this._reels = builder.build();

		new ActionPanel(stage, recources, this._repository);
		new Paytable(stage, this._repository, new SpinToPaytableResult());

	}

	get repository() {

		return this._repository;
	}

	update(delta) {
		this._reels.update(delta);
		this._renderEngine.renderReelItems(this._reels.getVisibleItems());
	}
}