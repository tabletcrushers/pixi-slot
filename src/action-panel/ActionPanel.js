import { Container, Sprite } from '../../pixi.js';
import Notificator from '../notificator/Notificator.js';
import MathUtils from '../utils/MathUtils.js';
import TextInput from '../3rd-party/text-input/TextInput.js';
import { Text, TextStyle } from '../../pixi.js';

const spinPrice = 1;

export default class ActionPanel extends Container {

    constructor(stage, recources, repository) {
		super();

		stage.addChild(this);

		this._symbolNames = ['1xbar', '2xbar', '3xbar', 'cherry', 'seven'];
		this._lineNames = ['top', 'middle', 'bottom'];

		this._repository = repository;
		this._spinButton = new Sprite(recources.spinButton.texture);
		this._spinButton.interactive = true;
		this._spinButton.buttonMode = true;
		this._spinButton.x = 20;
		this._spinButton.y = 500;
		this.addChild(this._spinButton);

		this._balanceField = this._createBalanceField();
		this._balanceField.text = '5000';
		this._repository.set('balance', 5000);
		this.addChild(this._balanceField);

		this._debugSymbols = [];
		this._debugLines = [];

		this._debugSymbol_0 = this._createDebugField(0, this._debugSymbols);
		this._debugSymbol_0.x = 30;
		this._debugSymbol_0.y = 270;
		this.addChild(this._debugSymbol_0);

		this._debugSymbol_1 = this._createDebugField(1, this._debugSymbols);
		this._debugSymbol_1.x = 170;
		this._debugSymbol_1.y = 270;
		this.addChild(this._debugSymbol_1);

		this._debugSymbol_2 = this._createDebugField(2, this._debugSymbols);
		this._debugSymbol_2.x = 310;
		this._debugSymbol_2.y = 270;
		this.addChild(this._debugSymbol_2);

		this._debugLine_0 = this._createDebugField(0, this._debugLines, 'Line name');
		this._debugLine_0.x = 30;
		this._debugLine_0.y = 320;
		this.addChild(this._debugLine_0);

		this._debugLine_1 = this._createDebugField(1, this._debugLines, 'Line name');
		this._debugLine_1.x = 170;
		this._debugLine_1.y = 320;
		this.addChild(this._debugLine_1);

		this._debugLine_2 = this._createDebugField(2, this._debugLines, 'Line name');
		this._debugLine_2.x = 310;
		this._debugLine_2.y = 320;
		this.addChild(this._debugLine_2);

		const style = {fill: ['#ffffff'], fontSize: 21}
		const debugHeader = new Text('-1', new TextStyle(style));
		debugHeader.text = 'Put names of symbols and their lines above\nEmpty or wrong means random\nSymbols: cherry, seven, 1xbar, 2xbar, 3xbar\nLines: top, middle, bottom';
		debugHeader.x = 25;
		debugHeader.y = 370;
		this.addChild(debugHeader);

		this._subscribe();
	}

	_setActivity(active) {
		this._spinButton.interactive = active;
		
		for (const child of this.children) {
			child.disabled = !active;
		}
	}

	_subscribe() {
		this._spinButton.on('pointerup', this._onSpinButtonClicked, this);
		this._balanceField.on('blur', this._updateRepositoryBalance, this);

		Notificator.instance.addListener('win', this._updateBallance, this);
		Notificator.instance.addListener('reelsStopped', this._onReelsStopped, this);
	}

	_onReelsStopped() {
		this._setActivity(true);
	}

	_updateRepositoryBalance() {
		let balance = parseInt(this._balanceField.text) || 0;
		balance = balance > 5000 ? 5000 : balance;
		balance = balance < 0 ? 0 : balance;
		this._balanceField.text = balance;
		this._repository.set('balance', parseInt(this._balanceField.text));
	}

	_updateBallance() {
		this._balanceField.text = this._repository.get('balance');
	}

	_createBalanceField() {
		const input = new TextInput({
			input: {
				fontSize: '36px',
				padding: '12px',
				width: '200px',
				color: '#26272E'
			},
			box: {
				default: {fill: 0xE8E9F3, rounded: 0, stroke: {color: 0xffb400, width: 3}},
				focused: {fill: 0xE1E3EE, rounded: 0, stroke: {color: 0xABAFC6, width: 3}},
				active: {fill: 0xDBDBDB, rounded: 0}
			}
		})
		
		input.x = 140;
		input.y = 505;

		return input;
	}

	_createDebugField(id, holder, placeholder = 'Symbol name') {
		const input = new TextInput({
			input: {
				fontSize: '14px',
				padding: '12px',
				width: '100px',
				color: '#26272E'
			},
			box: {
				default: {fill: 0xE8E9F3, rounded: 0, stroke: {color: 0xffb400, width: 3}},
				focused: {fill: 0xE1E3EE, rounded: 0, stroke: {color: 0xABAFC6, width: 3}},
				active: {fill: 0xDBDBDB, rounded: 0}
			}
		});

		input.id = id;
		input.placeholder = placeholder;
		input.x = 140;
		input.y = 505;

		holder.push(input);

		return input;
	}

	_generateResult() {
		const result = [];
		for (const i in this._debugSymbols) {
			const symbolField = this._debugSymbols[i];
			const lineField = this._debugLines[i];
			
			const symbol = this._checkSymbolName(symbolField.text) ? symbolField.text : this._getRandomSymbol();
			const line = this._checkLineName(lineField.text) ? lineField.text : this._getRandomLine();

			result.push({textureName: symbol, winLine: line});
		}

		return result;
	}

	_checkSymbolName(name) {

		return this._symbolNames.indexOf(name) > -1;
	}

	_checkLineName(name) {

		return this._lineNames.indexOf(name) > -1;
	}

	_onSpinButtonClicked() {
		const balance = this._repository.get('balance');
		if (balance <= spinPrice) {

			return;
		}

		this._repository.set('balance', balance - spinPrice);
		this._updateBallance();
		this._setActivity(false);

		Notificator.instance.notify('reelsStarted');

		const result = this._generateResult();
		this._repository.set('spinResult', result);

		setTimeout(() => { Notificator.instance.notify('spinResultReceived', result); }, 2000);
	}

	_getRandomSymbol() {
		const textureName = this._symbolNames[MathUtils.getRandomInt(0, 4)];
		
		return textureName;
	}
	
	_getRandomLine() {
		const winLine = this._lineNames[MathUtils.getRandomInt(0, 2)];

		return winLine;
	}
}