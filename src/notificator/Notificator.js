export default class Notificator {
	constructor() {
		this._notifications = new Map();
	}

	static get instance() {

		this._instance = Boolean(this._instance) ? this._instance : new Notificator();
		
		return this._instance;
	}

	addListener(name, callback, context) {
		let listeners = this._notifications.get(name);
		if (!Boolean(listeners)) {
			listeners = [];
			this._notifications.set(name, listeners);
		}
		listeners.push({ callback, context });
	}

	removeListener(name, callback) {
		const listeners = this._notifications.get(name);
		let index;
		for (const listener of listeners) {
			if (listener.callback == callback) {
				index =  listeners.indexOf(listener);
			}
		}

		listeners.splice(index, 1);
	}

	notify(name, ...args) {
		const listeners = this._notifications.get(name);

		for (const listener of listeners) {
			listener.callback.bind(listener.context)(...args);
		}
	}
}