export default class SpinToPaytableResult {
	convert(spinResult) {
		this._spinResult = spinResult;

		if (this._getAmount('cherry') > 2) {
			if (this._getLineEquality('top')) {

				return 2000;
			}
			if (this._getLineEquality('middle')) {

				return 1000;
			}
			if (this._getLineEquality('bottom')) {

				return 4000;
			}
		}

		if (this._getAmount('seven') > 2) {

			return 150;
		}

		if ((this._getAmount('seven') + this._getAmount('cherry') > 2) && this._getAmount('seven') < 3 && this._getAmount('cherry') < 3) {

			return 75;
		}

		if (this._getAmount('3xbar') > 2 && this._isThereAnyFullLine()) {

			return 50;
		}

		if (this._getAmount('2xbar') > 2 && this._isThereAnyFullLine()) {

			return 20;
		}

		if (this._getAmount('1xbar') > 2 && this._isThereAnyFullLine()) {

			return 10;
		}

		if (this._getAmount('1xbar') + this._getAmount('2xbar') + this._getAmount('3xbar') > 2) {

			return 5;
		}

		return 0;
	}

	_getAmount(name) {

		return this._spinResult.reduce((amount, result) => result.textureName === name ? amount + 1 : amount, 0);
	}

	_getLineEquality(line) {
		
		for (const result of this._spinResult) {
			if (line != result.winLine) {

				return false;
			}
		}

		return true;
	}

	_isThereAnyFullLine() {

		return this._getLineEquality('top') || this._getLineEquality('middle') || this._getLineEquality('bottom');
	}
}