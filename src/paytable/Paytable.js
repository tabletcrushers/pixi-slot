import { Container, Text, TextStyle } from '../../pixi.js';
import Notificator from '../notificator/Notificator.js';

export default class Paytable extends Container {
	constructor(stage, repository, spinResultConverter) {
		super();
		stage.addChild(this);
		this._payouts = this._createPayouts();
		this.x = 450;
		this.y = 0;
		this.scale.x = this.scale.y = .8;
		this._repository = repository;
		this._spinResultConverter = spinResultConverter;

		this._subscribe();
	}

	_subscribe() {
		Notificator.instance.addListener('reelsStopped', this._onReelsStopped, this);
	}

	_onReelsStopped() {
		const spinResult = this._repository.get('spinResult');

		const win = this._spinResultConverter.convert(spinResult);
		const payout = this._payouts.get(win);

		if (!Boolean(payout)) {

			return;
		}

		let interval = setInterval(() => {
			if (payout.visible) {
				payout.visible = false;
			}
			else {
				payout.visible = true;
			}
		}, 100);

		setTimeout(() => clearInterval(interval), 3000);

		const balance = this._repository.get('balance') + win;
		this._repository.set('balance', balance);

		Notificator.instance.notify('win');
	}

	_reset() {

	}

	_createPayouts() {
		const payouts = new Map();

		const offset = 25;
		payouts.set(4000, this._createPayout(4000, offset * 1));
		payouts.set(2000, this._createPayout(2000, offset * 2));
		payouts.set(1000, this._createPayout(1000, offset * 3));
		payouts.set(150, this._createPayout(150, offset * 4));
		payouts.set(75, this._createPayout(75, offset * 5));
		payouts.set(50, this._createPayout(50, offset * 6));
		payouts.set(20, this._createPayout(20,offset * 7));
		payouts.set(10, this._createPayout(10, offset * 8));
		payouts.set(5, this._createPayout(5, offset * 9));
		
		return payouts;
	}

	_createPayout(amount, y) {
		const style = {fill: ['#ffffff']};
		const payout = new Text(amount, new TextStyle(style));
		payout.y = y;

		this.addChild(payout);

		return payout;
	}
}