import { Application } from './pixi.js';
import Host from './src/host/Host.js';


const app = new Application({width: 510});
document.body.appendChild(app.view);

app.loader
	.add('1xbar', './assets/BAR.png')
	.add('2xbar', './assets/2xBAR.png')
	.add('3xbar', './assets/3xBAR.png')
	.add('seven', './assets/7.png')
	.add('cherry', './assets/cherry.png')
	.add('spinButton', './assets/spin-button.png')
	.load(launch);
	


function launch() {
	const host = new Host(app.stage, app.loader.resources);

	app.ticker.add(host.update, host);
	
	/*app.ticker.add((delta) => {
		host.update(delta);
	});*/
}